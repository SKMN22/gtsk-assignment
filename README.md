# GtskAssignment

This project was generated with [Angular CLI](https://github.com/angular/angular-cli) version 14.1.0.

## Development server

Run `ng serve` for a dev server. Navigate to `http://localhost:4200/`. The application will automatically reload if you change any of the source files.

This is assignment for Green Tube Slovakia. It is basic single page app with mocked user data.

Creation Date: 28.7.2022 cca(5 hours of coding).

By Roman Kvocka 