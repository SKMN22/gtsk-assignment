export interface Task {
  title: string;
  description: string;
  assignTo: string;
  dueDate?: string;
  creationDate: string;
  updatedDate?: string;
}
