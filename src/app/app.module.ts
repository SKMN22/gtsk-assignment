import { NgModule } from '@angular/core';
import { BrowserModule } from '@angular/platform-browser';

import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { MainTableComponent } from './components/main-table/main-table.component';
import { ModalWindowComponent } from './components/modal-window/modal-window.component';
import { TableModule } from 'primeng/table';
import { ButtonModule } from 'primeng/button';
import { DynamicDialogModule } from 'primeng/dynamicdialog';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { ReactiveFormsModule } from '@angular/forms';
import {ProgressSpinnerModule} from 'primeng/progressspinner';

@NgModule({
  declarations: [AppComponent, MainTableComponent, ModalWindowComponent],
  imports: [
    AppRoutingModule,
    BrowserModule,
    TableModule,
    ButtonModule,
    DynamicDialogModule,
    BrowserAnimationsModule,
    ProgressSpinnerModule,
    ReactiveFormsModule,
  ],
  entryComponents: [ModalWindowComponent],
  providers: [],
  bootstrap: [AppComponent],
})
export class AppModule {}
