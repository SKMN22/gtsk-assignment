import { DatePipe } from '@angular/common';
import { Component, OnInit } from '@angular/core';
import {
  AbstractControl,
  FormControl,
  FormGroup,
  Validators,
} from '@angular/forms';
import { DynamicDialogRef } from 'primeng/dynamicdialog';
import { DynamicDialogConfig } from 'primeng/dynamicdialog';
import { Task } from 'src/app/interfaces/task.interface';

@Component({
  templateUrl: './modal-window.component.html',
  styleUrls: ['./modal-window.component.scss'],
  providers: [DatePipe],
})
export class ModalWindowComponent implements OnInit {
  task: Task | undefined;
  type: string = '';
  formGroup!: FormGroup;

  titleFormControl!: AbstractControl | null;
  descriptionFormControl!: AbstractControl | null;
  assignToFormControl!: AbstractControl | null;
  updatedDate!: AbstractControl | null;

  constructor(
    private readonly datePipe: DatePipe,
    public ref: DynamicDialogRef,
    public config: DynamicDialogConfig
  ) {}

  ngOnInit(): void {
    this.task = this.config.data.task;
    this.type = this.config.data.type;
    this.formGroup = new FormGroup({
      title: new FormControl(this.task?.title || null, [
        Validators.maxLength(128),
        Validators.required,
      ]),
      description: new FormControl(this.task?.description || null, [
        Validators.maxLength(1024),
        Validators.required,
      ]),
      assignTo: new FormControl(this.task?.assignTo || null, [
        Validators.maxLength(128),
        Validators.required,
      ]),
      creationDate: new FormControl(this.task?.creationDate || null),
      updatedDate: new FormControl(this.task?.updatedDate || null),
      dueDate: new FormControl(this.task?.dueDate || null),
    });
    this.getFormControllers();
  }

  getFormControllers(): void {
    this.titleFormControl = this.formGroup.get('title');
    this.descriptionFormControl = this.formGroup.get('description');
    this.assignToFormControl = this.formGroup.get('assignTo');
    this.updatedDate = this.formGroup.get('updatedDate');
  }

  save(): void {
    const newDate = new Date();
    this.updatedDate?.setValue(this.datePipe.transform(newDate, 'yyyy-MM-dd'));
    this.ref.close({ ...this.formGroup.getRawValue() });
  }

  close(): void {
    this.ref.close();
  }

  delete(): void {
    this.ref.close('delete');
  }
}
