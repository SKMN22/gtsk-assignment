import { Component, OnDestroy, OnInit } from '@angular/core';
import { forkJoin, Subscription } from 'rxjs';
import { Task } from 'src/app/interfaces/task.interface';
import { TableServiceService } from 'src/app/services/table-service.service';
import { DialogService } from 'primeng/dynamicdialog';
import { ModalWindowComponent } from '../modal-window/modal-window.component';

@Component({
  selector: 'app-main-table',
  templateUrl: './main-table.component.html',
  styleUrls: ['./main-table.component.scss'],
  providers: [DialogService],
})
export class MainTableComponent implements OnInit, OnDestroy {
  cols: string[] = [];
  tasks: Task[] = [];

  forkJoinSubscription$!: Subscription;

  constructor(
    private readonly tableService: TableServiceService,
    private readonly dialogService: DialogService
  ) {}

  ngOnInit(): void {
    this.forkJoinSubscription$ = forkJoin({
      tasks: this.tableService.getTasks(),
      cols: this.tableService.getColumns(),
    }).subscribe(({ tasks, cols }) => {
      this.tasks = tasks;
      this.cols = cols;
    });
  }

  ngOnDestroy(): void {
    this.forkJoinSubscription$.unsubscribe();
  }

  handleButtonClick(type: string, task: Task): void {
    const index = this.tasks.findIndex((value) => value === task);
    const ref = this.dialogService.open(ModalWindowComponent, {
      data: {
        task: task,
        type: type,
      },
      header: task.title,
      width: '70%',
    });

    ref.onClose.subscribe((result: Task | string) => {
      if (result) {
        if (result === 'delete') {
          this.tasks = this.tableService.deleteTask(index);
          return;
        }
        if (typeof result !== 'string')
          this.tasks = this.tableService.editTask(result, index);
      }
    });
  }
}
