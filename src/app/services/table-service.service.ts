import { Injectable } from '@angular/core';
import { delay, Observable, of } from 'rxjs';
import { Task } from '../interfaces/task.interface';
import { TASKS } from '../../mock-data/mock-users';

@Injectable({
  providedIn: 'root',
})
export class TableServiceService {
  tasks: Task[] = [];
  constructor() {
    this.tasks = TASKS;
  }

  getColumns(): Observable<string[]> {
    const columns = ['Title', 'Assign to', 'Due date', 'Action'];
    return of(columns).pipe(delay(1000));
  }

  getTasks(): Observable<Task[]> {
    return of(this.tasks).pipe(delay(1000));
  }

  editTask(task: Task, index: number): Task[] {
    this.tasks[index] = task;
    return this.tasks;
  }

  deleteTask(index: number): Task[] {
    this.tasks.splice(index, 1);
    return this.tasks;
  }
}
