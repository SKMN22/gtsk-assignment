import { Task } from 'src/app/interfaces/task.interface';

export const TASKS: Task[] = [
  {
    title: 'A smile in the mind',
    assignTo: 'Roman',
    dueDate:  "2013-06-23",
    description: 'Description 123',
    creationDate: "2013-06-23",
  },
  {
    title: 'The Power of Now',
    assignTo: 'Matej',
    dueDate: "2013-06-23",
    description: 'Description 123',
    creationDate: "2013-06-23",
  },
  {
    title: 'Lean In',
    assignTo: 'Zdeno',
    dueDate: "2013-06-23",
    description: 'Description 123',
    creationDate: "2013-06-23",
  },
  {
    title: 'The Brain That Changes Itself',
    assignTo: 'Marek',
    dueDate: "2013-06-23",
    description: 'Description 123',
    creationDate: "2013-06-23",
  },
  {
    title: 'Men are from Mars Women are from Venus',
    assignTo: 'Jozo',
    dueDate: "2013-06-23",
    description: 'Description 123',
    creationDate: "2013-06-23",
  },
  {
    title: `Andrea Agassi's OPEN`,
    assignTo: 'Paprikas',
    dueDate: "2013-06-23",
    description: 'Description 123',
    creationDate: "2013-06-23",
  },
  {
    title: 'The Electronic Swagman',
    assignTo: 'Marian',
    dueDate: "2013-06-23",
    description: 'Description 123',
    creationDate: "2013-06-23",
  },
];
